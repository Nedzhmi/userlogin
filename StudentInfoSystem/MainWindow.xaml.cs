﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UserLogin;

namespace StudentInfoSystem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            btnTestDataExit.Visibility = Visibility.Hidden;
            DataContext = new MainFormVM();
        }


        private void clearTextBoxes()
        {
            foreach (var control in personalDataGrid.Children)
            {
                if (control is TextBox)
                {
                    TextBox textBox = (TextBox)control;
                    textBox.Text = "";
                }
            }

            foreach (var control in studentInfoGrid.Children)
            {
                if (control is TextBox)
                {
                    TextBox textBox = (TextBox)control;
                    textBox.Text = "";
                }
            }
        }

        private void setIsEnabledOnAllGridChildren(bool isEnabledValue)
        {
            foreach(var control in personalDataGrid.Children)
            {
                UIElement el = control as UIElement;
                el.IsEnabled = isEnabledValue;
            }

            foreach (var control in studentInfoGrid.Children)
            {
                UIElement el = control as UIElement;
                el.IsEnabled = isEnabledValue;
            }
        }

        private void btnTextData_Click(object sender, RoutedEventArgs e)
        {
            btnTestDataExit.Visibility = Visibility.Visible;
            btnTestDataLogin.Visibility = Visibility.Hidden;

            MessageBox.Show("Програмата е в тестов режим!");
            setIsEnabledOnAllGridChildren(true);

            bool studentsListEmpty = TestStudentsIfEmpty();
            if (studentsListEmpty)
            {
                CopyTestStudents();
                CopyTestUsers();
                MessageBox.Show("Empty students list!");
            }
        }

        private void btnTestDataExit_Click(object sender, RoutedEventArgs e)
        {
            clearTextBoxes();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow loginWindow = new LoginWindow();
            loginWindow.ShowDialog();
        }

        public void loadStudent()
        {
            btnTestDataLogin.Visibility=Visibility.Hidden;
            btnTestDataExit.Visibility=Visibility.Hidden;
        }

        public bool TestStudentsIfEmpty()
        {
            StudentInfoContext context = new StudentInfoContext();
            IEnumerable<Student> queryStudents = context.Students;
            int countStudents = queryStudents.Count();
            return countStudents == 0 ? true : false;
        }

        public void CopyTestStudents()
        {
            StudentInfoContext context = new StudentInfoContext();
            foreach (Student st in StudentData.allStudents)
            {
                context.Students.Add(st);
            }
            context.SaveChanges();
        }

        private void CopyTestUsers()
        {
            UserContext context = new UserContext();
            foreach (User user in UserData.TestUsers)
            {
                context.Users.Add(user);
            }
            context.SaveChanges();
        }
    }
}
