﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace StudentInfoSystem
{
    public class LoginCommand : ICommand
    {
        private LoginModel _loginModel;
        private readonly Action _closeWindowAction;

        public LoginCommand(LoginModel loginModel, Action closeWindowAction)
        {
            _loginModel = loginModel;
            _closeWindowAction = closeWindowAction;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }
        public void Execute(object parameter)
        {
            UserLogin.User userToLogin = UserLogin.UserData.IsUserPassCorrect(_loginModel.Username, _loginModel.Password);
            if (userToLogin == null)
            {
                MessageBox.Show("Invalid username or password!");
                return;
            }

            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            mainWindow.loadStudent();
            _closeWindowAction();
        }

        public event EventHandler CanExecuteChanged
        {
            add {}
            remove {}
        }
    }
}
