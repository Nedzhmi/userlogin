﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentInfoSystem
{
    internal class StudentData
    {
        public static List<Student> allStudents { get; private set; }

        public static void InitializeStudentData()
        {
            Student st = new Student();
            st.firstName = "Nedzhmi";
            st.middleName = "Ismet";
            st.lastName = "Parov";
            st.faculty = "FCST";
            st.major = "CSE";
            st.degree = Degree.BACHELOR;
            st.status = StudentStatus.ACTIVE;
            st.facultyNumber = "121219119";
            st.year = 3;
            st.studentGrouping = 1;
            st.group = 29;

            allStudents = new List<Student>();
            allStudents.Add(st);
        }

        public static Student isThereStudent(string facNum)
        {
            StudentInfoContext context = new StudentInfoContext();
            Student result = (from st in context.Students where st.facultyNumber == facNum select st).First();
            return result;
        }
    }
}
