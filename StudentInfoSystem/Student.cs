﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentInfoSystem
{
    public class Student
    {
        public int StudentId { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public string faculty { get; set; }
        public string major { get; set; }
        public Degree degree { get; set; }
        public StudentStatus status { get; set; }
        public string facultyNumber { get; set; }
        public int year { get; set; }
        public int studentGrouping { get; set; }
        public int group { get; set; }
    }
}
