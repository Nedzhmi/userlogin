﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserLogin;

namespace StudentInfoSystem
{
    internal class StudentValidation
    {

        public Student getStudentDataByUser(User user)
        {
            string userFacultyNum = user.facultyNumber;
            if (String.Equals(userFacultyNum, String.Empty)) return null;

            IEnumerable<Student> foundStudents = from student in StudentData.allStudents where String.Equals(student.facultyNumber, userFacultyNum) select student;
            return foundStudents.DefaultIfEmpty(null).First();
            
        }
    }
}
