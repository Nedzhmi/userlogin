﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFHello
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            ListBoxItem[] newItems = new ListBoxItem[2];
            for (int i = 0; i < newItems.Length; i++)
            {
                newItems[i] = new ListBoxItem();
            }
            newItems[0].Content = "James";
            newItems[1].Content = "David";
            foreach(ListBoxItem item in newItems)
            {
                peopleListBox.Items.Add(item);
            }
            peopleListBox.SelectedItem = newItems[0];
        }

        private void btnHello_Click(object sender, RoutedEventArgs e)
        {
            string userName = txtName.Text;
            
            if(userName.Length < 2)
            {
                MessageBox.Show("Името трябва да е с дължина от поне два символа!");
                return;
            }
            MessageBox.Show("Здрасти, " + txtName.Text + " това е твоята първа програма на Visual Studio 2012!");
        }

        private void nFactBtn_Click(object sender, RoutedEventArgs e)
        {
            string nValue = txtN.Text;
            int num;
            if (!int.TryParse(nValue, out num))
            {
                MessageBox.Show("Cannot parse the value for n! Invalid input!");
                return;
            }
            int nFactorial = 1;
            for(int i = num; i > 1; i--)
            {
                nFactorial *= i;
            }
            MessageBox.Show("n! is: " + nFactorial);
        }

        private void nPowerBtn_Click(object sender, RoutedEventArgs e)
        {
            string nValue = txtN.Text;
            string yValue = txtY.Text;

            double n, y;

            if (!double.TryParse(nValue, out n))
            {
                MessageBox.Show("Cannot parse the value for n! Invalid input!");
                return;
            }

            if (!double.TryParse(yValue, out y))
            {
                MessageBox.Show("Cannot parse the value for y! Invalid input!");
                return;
            }

            double powResult = Math.Pow(n, y);
            MessageBox.Show("n^y is: " + powResult);
        }

        private void btnShowCurrentlySelectedPerson_Click(object sender, RoutedEventArgs e)
        {
            string greetingMessage = (peopleListBox.SelectedItem as ListBoxItem).Content.ToString();
            //MessageBox.Show("Hi " + greetingMessage);
            MyMessage anotherWindow = new MyMessage();
            anotherWindow.Show();
        }
    }
}
