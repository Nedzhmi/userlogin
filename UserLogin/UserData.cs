﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserLogin
{
    public static class UserData
    {
        private static List<User> _testUsers;

        public static List<User> TestUsers
        {
            get
            {
                ResetTestUserData();
                return _testUsers; 
            }
            set { }
        }

        private static void ResetTestUserData()
        {
            if (_testUsers != null) return;
            _testUsers = new List<User>(3);

            for(int i = 0; i < _testUsers.Capacity; i++)
            {
                _testUsers.Add(new User());
            }

            _testUsers[0].username = "Admin";
            _testUsers[0].password = "Pass123";
            _testUsers[0].facultyNumber = "121219";
            _testUsers[0].role = UserRoles.ADMIN;
            _testUsers[0].created = DateTime.Now;
            _testUsers[0].activeUntil = DateTime.MaxValue;

            _testUsers[1].username = "Student1";
            _testUsers[1].password = "Parola1";
            _testUsers[1].facultyNumber = "1234567";
            _testUsers[1].role = UserRoles.STUDENT;
            _testUsers[1].created = DateTime.Now;
            _testUsers[1].activeUntil = DateTime.MaxValue;

            _testUsers[2].username = "Student2";
            _testUsers[2].password = "Parola2";
            _testUsers[2].facultyNumber = "9484723";
            _testUsers[2].role = UserRoles.STUDENT;
            _testUsers[2].created = DateTime.Now;
            _testUsers[2].activeUntil = DateTime.MaxValue;
        }

        public static User IsUserPassCorrect(string username, string password)
        {
            UserContext context = new UserContext();
            IEnumerable<User> users = (from user in context.Users where user.username.Equals(username) && user.password.Equals(password) select user);
            User userToReturn = users.DefaultIfEmpty(null).First();
            return userToReturn;
        }

        public static void SetUserActiveTo(string username, DateTime dateTime)
        {
            UserContext context = new UserContext();
            User usr = (from u in context.Users where u.username.Equals(username) select u).First();
            usr.activeUntil = dateTime;
            context.SaveChanges();
            Logger.LogActivity(username + "'s changed active until " + dateTime);
        }

        public static void assignUserRole(string username, UserRoles role)
        {
            UserContext context = new UserContext();
            User usr = (from u in context.Users where u.username.Equals(username) select u).First();
            usr.role = role;
            context.SaveChanges();
            Logger.LogActivity(username + "'s user role changed to " + role);
        }
    }
}
