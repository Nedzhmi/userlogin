﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace UserLogin
{
    internal class Program
    {
        static void Main(string[] args)
        {
            DateTime yesterday = DateTime.Now.AddDays(-1);
            Logger.clearOldLogs(yesterday);

            Console.Write("Please enter username: ");
            string username = Console.ReadLine();
            Console.Write("Please enter password: ");
            string password = Console.ReadLine();

            LoginValidation validation = new LoginValidation(username, password, handleError);
            User newUser = null;

            if (validation.validateUserInput(ref newUser))
            {
                Console.WriteLine("Admin details:");
                Console.WriteLine("Username: " + newUser.username);
                Console.WriteLine("Faculty number: " + newUser.facultyNumber);
                Console.WriteLine("Role: " + newUser.role);
            }

            switch (LoginValidation.currentUserRole)
            {
                case UserRoles.ADMIN:
                    Console.WriteLine("Admin login successful!");
                    readUserAction();
                    break;
                case UserRoles.ANONYMOUS:
                    Console.WriteLine("Login failed!");
                    break;
                case UserRoles.INSPECTOR:
                    Console.WriteLine("Inspector login successful!");
                    break;
                case UserRoles.PROFESSOR:
                    Console.WriteLine("Proffesor login successful!");
                    break;
                case UserRoles.STUDENT:
                    Console.WriteLine("Student login successful!");
                    break;
            }

        }

        public static void handleError(string errorMsg)
        {
            Console.WriteLine("An error has occured: " + errorMsg);
        }

        private static void showAdminMenu()
        {
            Console.WriteLine("Admin menu (please select from options below)");
            Console.WriteLine("\t0 -> Exit");
            Console.WriteLine("\t1 -> Change user's role");
            Console.WriteLine("\t2 -> Change user's activity");
            Console.WriteLine("\t3 -> List users");
            Console.WriteLine("\t4 -> List log file");
            Console.WriteLine("\t5 -> Log current session activity");
        }

        private static void readUserAction()
        {
            int action = -1;
            do
            {
                showAdminMenu();
                Console.Write("Please select from the listed actions: ");
                int actionChosen = Int32.Parse(Console.ReadLine());

                if (actionChosen < 0 || actionChosen > 5)
                {
                    Console.WriteLine("Invalid selected action!");
                    continue;
                }
                action = actionChosen;
            } while (action == -1);

            switch (action)
            {
                case 0:
                    return;
                case 1:
                    changeUserRole();
                    break;
                case 2:
                    changeUserActivity();
                    break;
                case 3:
                    listUsers();
                    break;
                case 4:
                    listLogFile();
                    break;
                case 5:
                    logCurrentSession();
                    break;
            }
        }

        private static string requestUsername()
        {
            Console.Write("Please enter username: ");
            return Console.ReadLine();
        }

        private static void printUserRoles()
        {
            Console.WriteLine("Available roles:");
            foreach(UserRoles role in Enum.GetValues(typeof(UserRoles)))
            {
                Console.WriteLine(role);
            }
        }

        private static UserRoles requestUserRole()
        {
            int role = -1;
            do
            {
                printUserRoles();
                Console.WriteLine("Select the desired role:");
                int chosenRole = Int32.Parse(Console.ReadLine());
                if (chosenRole < 0 || chosenRole > (int)Enum.GetValues(typeof(UserRoles)).Cast<UserRoles>().Last())
                {
                    Console.WriteLine("Invalid selected action!");
                    continue;
                }
                role = chosenRole;
            } while (role == -1);
            return (UserRoles) role;
        }

        private static void changeUserRole()
        {
            string username = requestUsername();
            UserRoles newRole = requestUserRole();
            UserData.assignUserRole(username, newRole);
        }

        private static void changeUserActivity()
        {
            string username = requestUsername();
            DateTime newDate = Convert.ToDateTime(Console.ReadLine());
            UserData.SetUserActiveTo(username, newDate);
        }

        private static void listUsers()
        {

        }

        public static void logCurrentSession()
        {
            StringBuilder sb = new StringBuilder();

            Console.Write("Enter a filter for the activities: ");
            string filterString = Console.ReadLine();
            foreach (string activityLine in Logger.getCurrentSessionActivities(filterString))
            {
                sb.Append(activityLine + "\n");
            }

            Console.WriteLine(sb.ToString());
        }

        public static void listLogFile()
        {
            IEnumerable<string> activities = Logger.getAllActivities();
            StringBuilder sb = new StringBuilder();
            foreach(string activityLine in activities)
            {
                sb.Append(activityLine + "\n");
            }
            Console.WriteLine(sb.ToString());
        }
    }
}
