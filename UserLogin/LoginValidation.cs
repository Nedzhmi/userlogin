﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserLogin
{
    public class LoginValidation
    {
        public static UserRoles currentUserRole { get; private set; }
        public static string currentUserUsername { get; private set; }
        private string _username;
        private string _password;
        private string _errorMessage;
        private ActionOnError _actionOnError;
        public delegate void ActionOnError(string errorMessage);

        public LoginValidation(string username, string password, ActionOnError actionOnError)
        {
            _username = username;
            _password = password;
            _actionOnError = actionOnError;
        }

        public bool validateUserInput(ref User user)
        {
            if (_username.Equals(String.Empty))
            {
                _errorMessage = "Username is empty!";
                _actionOnError(_errorMessage);
                return false;
            }
            if (_password.Equals(String.Empty))
            {
                _errorMessage = "Password is empty!";
                _actionOnError(_errorMessage);
                return false;
            }
            if (_username.Length < 5)
            {
                _errorMessage = "Username cannot be less than 5 symbols!";
                _actionOnError(_errorMessage);
                return false;
            }
            if (_password.Length < 5)
            {
                _errorMessage = "Password cannot be less than 5 symbols!";
                _actionOnError(_errorMessage);
                return false;
            }

            User checkedUser = UserData.IsUserPassCorrect(_username, _password);
            if (checkedUser != null)
            {
                user = new User();
                user.username = checkedUser.username;
                user.password = checkedUser.password;
                user.facultyNumber = checkedUser.facultyNumber;
                user.role = checkedUser.role;
                currentUserRole = (UserRoles) user.role;
                currentUserUsername = user.username;
                Logger.LogActivity("Logged successfully!");
                return true;
            }

            _errorMessage = "No such user found!";
            _actionOnError(_errorMessage);
            currentUserRole = UserRoles.ANONYMOUS;

            return false;
        }
    }
}
