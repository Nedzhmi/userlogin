﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace UserLogin
{
    public static class Logger
    {
        private static List<string> currentSessionActivities = new List<string>();

        public static void LogActivity(string activity)
        {
            string activityLine = DateTime.Now.ToFileTime().ToString() + ';' + LoginValidation.currentUserUsername + ';' 
                + LoginValidation.currentUserRole + ';' + activity;

            currentSessionActivities.Add(activityLine);

            File.AppendAllText("logger.log", activityLine + "\n");
        }

        public static IEnumerable<string> getAllActivities()
        {
            List<string> loggedActivities = new List<string>();
            string logs = File.ReadAllText("logger.log");

            if (!String.Equals(logs, String.Empty))
            {
                string[] allLogs = logs.Split('\n');
                foreach (string log in allLogs)
                {
                    if (String.Equals(log, String.Empty)) continue;

                    loggedActivities.Add(log);
                }
            }

            return loggedActivities;
        }

        public static IEnumerable<string> getCurrentSessionActivities(string filter)
        {
            List<string> filteredActivities = (from activity in currentSessionActivities where activity.Contains(filter) select activity).ToList();
            return filteredActivities;
        }

        public static void clearOldLogs(DateTime dateValueBefore)
        {
            string fileContent = File.ReadAllText("logger.log");
            if (String.Equals(fileContent, String.Empty)) return;

            string[] allLogs = fileContent.Split('\n');
            List<string> newLogs = new List<string>();

            foreach(string log in allLogs)
            {
                if (String.Equals(log, String.Empty)) continue;

                string timestampString = log.Split(';')[0];
                long timestamp = Convert.ToInt64(timestampString);
                DateTime loggedDate = DateTime.FromFileTime(timestamp);
                if(loggedDate >= dateValueBefore)
                {
                    newLogs.Add(log);
                }
            }
            string newLogString = convertLogsToString(newLogs);
            writeNewLog(newLogString);
        }

        private static string convertLogsToString(List<string> newLogs)
        {
            StringBuilder sb = new StringBuilder();
            foreach(string log in newLogs)
            {
                sb.Append(log + "\n");
            }
            return sb.ToString();
        }

        private static void writeNewLog(string newLogString)
        {
            File.WriteAllText("logger.log", newLogString);
        }
    }
}
